package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest3 {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_image() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_PNG))
                .andReturn();

        byte [] expected = new byte[] {(byte) 137, 80, 78, 71, 13, 10, 26, 10};

        byte[] contentAsByteArray = mvcResult.getResponse().getContentAsByteArray();

        byte[] actual = Arrays.copyOfRange(contentAsByteArray, 0, 8);

        assertArrayEquals(expected, actual);
    }
}


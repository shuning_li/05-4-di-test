package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest2 {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    GameLevelService gameLevelService;

    @Mock
    MockHttpServletResponse response;

//    @Mock
//    GameLevelService gameLevelService;

//    @TestConfiguration
//    static class MockGameLevelService {
//        @Bean
//        @Primary
//        public GameLevelService mockGameLevelService() throws IOException {
//            GameLevelService mock = mock(GameLevelService.class);
//            byte[] expected = new byte[]{1, 2, 3};
//            when(mock.renderMaze(10, 10, "color")).thenReturn(expected);
//            return mock;
//        }
//    }

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void should_render_maze() throws Exception {
        byte[] expected = new byte[]{1, 2, 3};
        when(gameLevelService.renderMaze(10, 10, "color")).thenReturn(expected);

        mockMvc.perform(get("/buffered-mazes/color"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(expected));
    }

    @Test
    void should_render_maze_and_the_params_are_correct() throws Exception {
        mockMvc.perform(get("/buffered-mazes/color?width=20&height=20"));

        verify(gameLevelService, times(1))
                .renderMaze(20, 20, "color");
    }
}

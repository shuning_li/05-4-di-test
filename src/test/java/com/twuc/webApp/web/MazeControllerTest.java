package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


class MazeControllerTest {

    private MazeController mazeController;

    @Mock
    GameLevelService gameLevelService;

    @Mock
    MockHttpServletResponse response;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mazeController = new MazeController(gameLevelService);
    }

    @Test
    void should_render_maze() throws IOException {
        byte [] expected = new byte[1];
        when(gameLevelService.renderMaze(10, 10, "grass")).thenReturn(expected);

        ResponseEntity<byte[]> responseEntity = mazeController.getMaze(10, 10, "grass");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MediaType.IMAGE_PNG, responseEntity.getHeaders().getContentType());
        assertArrayEquals(expected, responseEntity.getBody());
    }

    @Test
    void should_render_maze_2() throws IOException {
        mazeController.getMaze2(response,10, 10, "grass");

        verify(gameLevelService, times(1))
                .renderMaze(response.getOutputStream(),10, 10, "grass");
        verify(response).setContentType(MediaType.IMAGE_PNG_VALUE);
    }

//    @Test
//    void should_render_maze_2_2() {
//
//    }

}